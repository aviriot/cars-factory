import React, { useEffect, useState } from 'react';
import BoutonMontage from '../components/BoutonMontage';
import BoutonProduction from '../components/BoutonProduction';
import ChampNbEmployes from '../components/ChampNbEmployes';

/** Style pour les boutons */
const styleButtonDiv = {
    display: 'flex', 
    flexDirection: 'row', 
    justifyContent: 'space-around',
    marginTop: '2%',
}

/** Style pour les compteurs */
const divCompteur = {
    marginTop: '2%',
    marginBottom: '2%'
}

/**
 * Vue principale de l'application. 
 * 
 * @version 1.0.1
 * @author Audrey Viriot
 */
export default function PagePrincipale() {

    /** Contient le nombre de modèle A produit */
    const [cptA, setCptA] = useState(0);
    /** Contient le nombre de modèle B produit */
    const [cptB, setCptB] = useState(0);

    /** Contient le nombre d'employés déterminés dans le champ ChampNbEmployes*/
    const [nbEmployes, setNbEmployes] = useState(0);

    /** Contient le nombre de pneus produit */
    const [nbPneus, setNbPneus] = useState(0);
    /** Contient le nombre de portes produite */
    const [nbPortes, setNbPortes] = useState(0);
    /** Contient le nombre de moteurs produit */
    const [nbMoteurs, setNbMoteurs] = useState(0);
    /** Contient le nombre de châssis produit */
    const [nbChassis, setNbChassis] = useState(0);

    /** Contient l'ensemble des éléments produit pouvant composer une voiture */
    const [listeComposantProduit, setListeComposantProduit] = useState({
        Pneus: nbPneus,
        Portes: nbPortes,
        Moteurs: nbMoteurs,
        Châssis: nbChassis
    })

    useEffect(() => {
        setListeComposantProduit({
            Pneus: nbPneus,
            Portes: nbPortes,
            Moteurs: nbMoteurs,
            Châssis: nbChassis
        })
    }, [nbPneus, nbPortes, nbMoteurs, nbChassis])

    /** Fonction permettant de construire l'objet contenant le nombre de pièce necessaire par modèles */
    const constructListComposantNecessaire = (nbPneus, nbPortes, nbMoteurs, nbChassis) => {
        var listeComposantModele = {
            Pneus: nbPneus,
            Portes: nbPortes,
            Moteurs: nbMoteurs,
            Châssis: nbChassis
        }
        return listeComposantModele;
    }

    /** Fonction permettant de mettre à jour les composants produits */
    const updateListeComposantProduit = (nbPneus, nbPortes, nbMoteurs, nbChassis) => {
        setNbPneus(nbPneus);
        setNbPortes(nbPortes);
        setNbMoteurs(nbMoteurs);
        setNbChassis(nbChassis); 
    }

    return (
        <div style={{ display: 'flex', flexDirection: 'column', width: '65%', margin: 'auto'}}>

            <h1>Palmeiras</h1>

            <div style={divCompteur}>
                Nombre de modèles A : {cptA}
                <br />
                <br />
                Nombre de modèles B : {cptB}
            </div>

            <ChampNbEmployes nbEmployes={nbEmployes} setNbEmployes={setNbEmployes} />

            <div style={styleButtonDiv}>
                <BoutonProduction nomComposant='pneus' tempsProduction={0.2} nbComposant={nbPneus} setNbComposant={setNbPneus} nbEmployes={nbEmployes} />
                <BoutonProduction nomComposant='portes' tempsProduction={1} nbComposant={nbPortes} setNbComposant={setNbPortes} nbEmployes={nbEmployes} />
                <BoutonProduction nomComposant='moteurs' tempsProduction={3} nbComposant={nbMoteurs} setNbComposant={setNbMoteurs} nbEmployes={nbEmployes} />
                <BoutonProduction nomComposant='chassis' tempsProduction={2} nbComposant={nbChassis} setNbComposant={setNbChassis} nbEmployes={nbEmployes} />
            </div>

            <div style={styleButtonDiv}>
                <BoutonMontage nomModele='A' tempsMontage={0.5} composantNecessaire={constructListComposantNecessaire (4, 2, 1, 1)} composantProduit={listeComposantProduit} updateComposantProduit={updateListeComposantProduit} nbEmployes={nbEmployes} cpt={cptA} setCpt={setCptA}/>
                <BoutonMontage nomModele='B' tempsMontage={0.6} composantNecessaire={constructListComposantNecessaire (6, 4, 1, 1)} composantProduit={listeComposantProduit} updateComposantProduit={updateListeComposantProduit} nbEmployes={nbEmployes} cpt={cptB} setCpt={setCptB}/>
            </div>

        </div>
    )

}