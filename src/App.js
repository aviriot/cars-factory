import logo from './logo.svg';
import './App.css';
import PagePrincipale from './views/PagePrincipale';

function App() {
  return (
    <div className="App">
      <PagePrincipale/>
    </div>
  );
}

export default App;
