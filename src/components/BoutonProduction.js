import * as React from 'react';
import { Button, Tooltip } from '@mui/material';

/**
 * Composant bouton permettant de produire une pièce (pneux, portes, moteurs, châssis)
 * 
 * @version 1.0.1
 * @author Audrey Viriot
 */
export default function BoutonProduction({ nomComposant, tempsProduction, nbComposant, setNbComposant, nbEmployes }) {

    /** Fonction déterminant le nombre de pièces pouvant être produites en fonction du nombre d'employés disponibles */
    const productionCapability = () => {
        var prodJournee = 1/tempsProduction;
        return prodJournee*nbEmployes; 
    }

    /** Fonction déterminant le contenu du 'Tooltip', renvoit un string  */
    const defineTitleTooltip = () => {
        return 'Possibilités de production : '+productionCapability()+' '+nomComposant;
    }

    /** Fonction appelée au clique sur le bouton, elle permet d'ajouter le nombre de nouveaux composants produits */
    const handleClick = () => {
        setNbComposant(nbComposant+productionCapability()); 
    }

    return (
        <Tooltip title={defineTitleTooltip()} placement='top'>
            <Button variant="outlined" onClick={handleClick}>
                Production de {nomComposant}
            </Button>
        </Tooltip>
    )

}