import React, { useEffect, useState } from 'react';
import { Button, Tooltip } from '@mui/material';

/**
 * Comnposant bouton permettant de monter une voiture. 
 * 
 * @version 1.0.1
 * @author Audrey Viriot
 */
export default function BoutonMontage({ nomModele, tempsMontage, composantNecessaire, composantProduit, updateComposantProduit, nbEmployes, cpt, setCpt }) {

    /** Variable contenant la couleurs du bouton */
    const [color, setColor] = useState('primary'); 
    /** Variable booléenne indiquant si un modèle peut-être monté ou non */
    const [isReady, setIsReady] = useState(false); 

    /** Fonction déterminant si les pièces necessaires au montage d'une voiture sont disponibles */
    const checkIfIsReady = () => {
        for(let i in composantProduit){
            if(composantProduit[i] < composantNecessaire[i]){
                setIsReady(false);
                setColor('primary');
                break;
            }else{
                setIsReady(true); 
                setColor('success');
            }
        }
    }

    useEffect(() => {
        checkIfIsReady();
    }, [composantProduit])

    /** Fonction déterminant en fonction du nombre de pièces disponibles et du nombre d'employés combien de voiture peuvent être montées */
    const productionCapability = () => {
        var prodPneu = Math.floor(Math.floor(composantProduit['Pneus'])/composantNecessaire['Pneus']);
        var prodPorte = Math.floor(Math.floor(composantProduit['Portes'])/composantNecessaire['Portes']); 
        var prodMoteur = Math.floor(Math.floor(composantProduit['Moteurs'])/composantNecessaire['Moteurs']);
        var prodChassis = Math.floor(Math.floor(composantProduit['Châssis'])/composantNecessaire['Châssis']); 

        var arrayMin = [prodPneu, prodPorte, prodMoteur, prodChassis];

        var nbProdParComposant = Math.min(...arrayMin); 

        var prodJournee = (1 / tempsMontage) * nbEmployes;

        if(nbProdParComposant <= prodJournee){
            return nbProdParComposant; 
        }else{
            return prodJournee
        }; 
    }

    /** Fonction déterminant le contenu du 'Tooltip', cette fonction renvoie un string */
    const defineTitleTooltip = () => {

        var result = '';

        for(let i in composantProduit){
            result += i + ' : ' + composantProduit[i] + ' ';
        }

        if(isReady){
            result += ' Possibilités de production : ' + Math.floor(productionCapability()) + ' modèles ' + nomModele;
        }else{
            result += ' Possibilités de production : ' + 0 + ' modèles ' + nomModele;
        }
        
        return result; 
    }

    /** Fonction appelé au clique sur le bouton, si des voitures peuvent être montées on soustraie les pièces utilisées au nombre de pièces produites */
    const handleClick = () => {
        if(isReady){
            var arrayTemp = [];
            setCpt(cpt+Math.floor(productionCapability()));
            for(let i in composantProduit){
                arrayTemp.push(composantProduit[i] - composantNecessaire[i]);
            }
            updateComposantProduit(arrayTemp[0], arrayTemp[1], arrayTemp[2], arrayTemp[3]);
            checkIfIsReady(); 
        } 
    }

    return (
        <Tooltip title={defineTitleTooltip()} placement='bottom'>
            <Button variant="contained" color={color} onClick={handleClick}>
                Montage de voiture modèle {nomModele}
            </Button>
        </Tooltip>
    )

}