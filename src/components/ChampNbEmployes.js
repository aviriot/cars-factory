import React from 'react';
import { makeStyles, TextField } from '@material-ui/core';

/** Style pour le champ 'Nombre d'employés' */
const useStyles = makeStyles((theme) => ({
    champStyle: {
        width: theme.spacing(30),
        marginRight: 'auto',
        marginLeft: 'auto'
    }
}));

/**
 * Composant représentant le champ de saisie permettant d'indiquer le nombre d'employés disponibles
 * 
 * @version 1.0.1
 * @author Audrey Viriot
 */
export default function ChampNbEmployes({ nbEmployes, setNbEmployes }) {

    /** Variables permettant d'utiliser le style définit plus haut (Material-UI) */
    const classes = useStyles();

    /** Fonction permettant d'enregistrer la valeurs du champ lorsque le champ est modifié */
    const handleChamp = (e) => {
        setNbEmployes(e.target.value);
    }

    return (
        <TextField
            required
            label="Nombre d'employés"
            type="number"
            className={classes.champStyle}
            InputProps={{ inputProps: { min: 0 } }}
            value={nbEmployes}
            onChange={handleChamp}
        />
    )

}