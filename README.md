# Documentation du projet Cars Factory

Ce projet est un exercice permettant de simuler la production de pièces de voitures ainsi que le montage de voitures dans une entreprise. 
Les pièces sont les suivantes : pneus, portes, moteurs et châssis. 
Deux modèles sont disponibles le A et le B. 
Chaque pièces possèdent un temps de production. 
Chaque modèles necessites un nombre précis de pièces ainsi qu'un temps de production. 
En fonction du nombre d'employés disponibles l'application calcul combien de pièces peuvent être fabriqué par jour.
Elle calcul aussi le nombre de voiture pouvant être montées par jour en fonction du nombre de pièces et d'employés disponibles. 

## Démarrage de l'application en local

Dans le répertoire du projet, vous pouvez exécuter :

### `yarn install`

Installe toutes les dépendances necessaires au bon fonctionnement du projet. 

### `yarn start`

Exécute l'application en mode développement. 
Ouvrez [http://localhost:3000](http://localhost:3000) pour voir l'application dans le navigateur. 


